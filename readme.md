to run this project

1- open android studio and run the emulator 

2- copy the emulator name (i.e: Nexus_5X_API_28_x86)

3- open nightwatch.json (In the root file) 
    - change the ("app": TO YOUR LOCAL PATH)
    - change the ("deviceName" & "avd" : TO THE NAME OF YOUR EMULATOR) 

4- npm install

5- run (react-native run-android)

6- open another terminal and run (Appium)

5- run this command nightwatch __tests__/androidTest.js -e androidNative
